﻿using System;
using System.Threading;

namespace Threads
{
    class Program
    {
        static void Main()
        {
            Exercise1();
            //Exercise2();
        }

        static void Exercise1()
        {
            string toWrite1 = "Una vegada hi havia un gat";
            string toWrite2 = "En un lugar de la Mancha";
            string toWrite3 = "Once upon a time in the west";

            Thread t1 = new Thread(() => WriteAndWaitOnSpace(toWrite1)),
                
            t2 = new Thread(() => {
                    t1.Join();
                    WriteAndWaitOnSpace(toWrite2);
                }),
            
            t3 = new Thread(() => {
                    t2.Join();
                    WriteAndWaitOnSpace(toWrite3);
                });

            t1.Start();
            t2.Start();
            t3.Start();
        }

        static void WriteAndWaitOnSpace(string toWrite)
        {
            for (int i = 0; i < toWrite.Length; i++)
            {
                Console.Write(toWrite[i]);
                if (toWrite[i] == ' ')
                    Thread.Sleep(1000);
            }

            Thread.Sleep(1000);
            Console.WriteLine();
        }

        static void Exercise2()
        {
            Fridge fridge = new Fridge();

            Thread t1 = new Thread(() => fridge.Fill("Annita")),
            
            t2 = new Thread(() => {
                    t1.Join();
                    fridge.Drink("Bad Bunny");
                }),
            
            t3 = new Thread(() => {
                    t2.Join();
                    fridge.Drink("Lil Nas X");
                }),
            
            t4 = new Thread(() => {
                    t3.Join();
                    fridge.Drink("Manuel Turizo");
                });

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();
        }
    }
}

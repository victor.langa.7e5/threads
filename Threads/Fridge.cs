﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Threads
{
    class Fridge
    {
        public int currentBeers;

        public Fridge() => currentBeers = 0;

        public void Fill(string filler)
        {
            int beersFilled = new Random().Next(0, 7);
            currentBeers = Math.Min(currentBeers + beersFilled, 9);
            Console.WriteLine("{0} fill the fridge with {1} beer(s) ({2} beer(s) left)", filler, beersFilled, currentBeers);
        }

        public void Drink(string drinker)
        {
            int beersDrunk = new Random().Next(0, 7);
            currentBeers = Math.Max(0, currentBeers - beersDrunk);
            Console.WriteLine("{0} drank {1} beer(s) ({2} beer(s) left)", drinker, beersDrunk, currentBeers);
        }
    }
}
